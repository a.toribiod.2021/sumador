#!/usr/bin/python

import random
import socket

my_port = 1234


def process(request):
    req = request.decode('utf-8')
    print(req)
    params = req.split()
    resource = params[1]
    components = resource.split('/')
    print(components)
    if components[1] == "suma":
        suma = int(components[2]) + int(components[3])
        response = "HTTP/1.1 200 OK\r\n"
        response += "Content-Type: text/html\r\n"
        response += "\r\n"
        response += "<html><body>" + f"<h1>Resultado: {suma}</h1>" + "</body></html>" + "\r\n"
    else:
        response = "HTTP/1.1 404 Not Found\r\n"

    return response.encode('utf-8')


def main():
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    mySocket.bind(('', my_port))
    mySocket.listen(5)
    random.seed()

    try:
        while True:
            print(f"Waiting for connections (port: {my_port})")
            (recvSocket, address) = mySocket.accept()
            request = recvSocket.recv(8*1024)
            print("HTTP request received:")
            response = process(request)
            recvSocket.send(response)
            recvSocket.close()

    except KeyboardInterrupt:
        print("Closing binded socket")
        mySocket.close()


if __name__ == "__main__":
    main()
